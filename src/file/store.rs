use std::path::Path;

use tokio::fs::OpenOptions;
// use tokio::{io::Seek, path::Path};

use tokio::fs::File;

use rocket::http::Status;
use rocket::State;

use tokio::io::{AsyncSeekExt, AsyncWriteExt};
use tracing::debug;
use tracing::{error, trace};

use crate::id::PasteId;
use crate::Paste;

// pub fn update_multipart(
//     boundary: &str,
//     paste: Data,
//     config: State<crate::ConfigState>,
// ) -> Result<(), Status> {
//     let mut multipart = Multipart::with_body(paste.open(), boundary);

//     loop {
//         let data = multipart.read_entry().unwrap();
//         match data {
//             Some(mut entry) => {
//                 let id = &entry.headers.name;

//                 if id.contains("/") || id.contains("\\") {
//                     return Err(Status::NotAcceptable);
//                 }

//                 let mut file = update_file(&config, id).unwrap();

//                 entry
//                     .data
//                     .save()
//                     .size_limit(None)
//                     .memory_threshold(0)
//                     .write_to(&mut file);
//             }
//             None => break,
//         }
//     }

//     Ok(())
// }

// pub fn update(
//     cont_type: &ContentType,
//     paste: Data,
//     config: State<crate::ConfigState>,
// ) -> Result<(), Status> {
//     let (_, boundary) = cont_type
//         .params()
//         .find(|&(k, _)| k == "boundary")
//         .ok_or_else(|| Err::<String, Status>(Status::BadRequest))
//         .unwrap();

//     update_multipart(boundary, paste, config)
// }
//
// async fn update_file(config: &State<crate::ConfigState>, id: &str) -> Result<File, Status> {
//     let filename = Path::new(&config.app_config.storage_dir).join(id);

//     if !filename.exists() {
//         return Err(Status::NotFound);
//     }

//     let file = OpenOptions::new()
//         .write(true)
//         .open(&filename)
//         .await
//         .unwrap();

//     Ok(file)
// }

#[tracing::instrument(skip(data, config))]
pub async fn store<'a>(
    data: crate::multipart::Form<'a>,
    config: &State<crate::ConfigState>,
) -> Result<Vec<Paste>, Status> {
    let mut pastes = Vec::new();

    for mut paste in data.into_inner() {
        trace!("storing file");
        let (mut file, id) = create_file(&config).await.unwrap();

        debug!("got paste as Data");
        // TODO check if write complete and do something with that?
        // let mut stream = v.open(10.gigabytes());

        // Process the field data chunks e.g. store them in a file.
        while let Some(chunk) = paste
            .chunk()
            .await
            .map_err(|_| Status::InternalServerError)?
        {
            file.write_all(&chunk).await.map_err(|err| {
                error!("failed to stream file to disk: {:?}", err);
                Status::InternalServerError
            })?;
        }

        // Go back to beginning of file for us to be able to read it again
        file.seek(std::io::SeekFrom::Start(0)).await.map_err(|e| {
            error!("failed to seek file: {:?}", e);
            Status::InternalServerError
        })?;

        let paste = Paste::from_file(id, &mut file).await?;
        trace!("paste: {:?}", paste);
        store_db(&config.db, &paste);

        pastes.push(paste);
    }

    Ok(pastes)
}

pub(crate) async fn create_file(
    config: &State<crate::ConfigState>,
) -> Result<(File, PasteId), Status> {
    let id = PasteId::new();
    let filename = Path::new(&config.app_config.storage_dir).join(&id.id);

    if filename.exists() {
        return Err(Status::Conflict);
    };

    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .truncate(true)
        .open(&filename)
        .await
        .map_err(|e| {
            error!("failed to create file: {:?}", e);
            Status::InternalServerError
        })?;

    Ok((file, id))
}

pub(crate) fn store_db(db: &sled::Db, paste: &Paste) {
    db.insert(&paste.id.id, bincode::serialize(&paste).unwrap())
        .unwrap();
    db.flush().unwrap();
}
